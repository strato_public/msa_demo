FROM openjdk:8-jdk-alpine

LABEL desc='msa app test'
LABEL email='dslim@strato.co.kr'

ARG SERVER_ENV=dev
ENV SERVER_ENV ${SERVER_ENV}
ENV JAVA_OPTS="-XX:PermSize=512m -XX:MaxPermSize=256m -Xmx4g -Xms2g"

EXPOSE 8080

COPY ./msa_demo-0.0.1-SNAPSHOT.jar app.jar

ENTRYPOINT ["java", "-Dspring.profiles.active=${SERVER_ENV}", "-jar","/app.jar"]

